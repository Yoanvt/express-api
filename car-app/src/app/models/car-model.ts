export class Car {
    id: number;
    brand: string;
    model: string;
    engine: string;
    enginestats: string;
    gearbox: string;
    horsepower: string;
    tyrebrand: string;
    color: string;
    year: string;
    date: Date;
    ImageUrl: string;
}
