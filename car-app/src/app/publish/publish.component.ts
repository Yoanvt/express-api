import { CarService } from './../services/car.service';
import { AllCars } from './../models/cardb-model';
import { Car } from './../models/car-model';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.scss']
})
export class PublishComponent implements OnInit {

  rForm: FormGroup;
  cars: AllCars[];
  models: string[];
  Car: Car;
  dateNow = Date.now();
  carId: string;
  receivedCar;

  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private fb: FormBuilder, private router: Router,
    private carService: CarService) {

    this.carId = this.activatedRoute.snapshot.paramMap.get('id');

    this.rForm = this.fb.group({
      brand: ['', Validators.required],
      model: ['', Validators.required],
      engine: ['', Validators.required],
      gearbox: ['', Validators.required],
      year: ['', Validators.required],
      enginestats: ['', Validators.required],
      horsepower: ['', Validators.required],
      tyrebrand: ['', Validators.required],
      color: ['', Validators.required],
      ImageUrl: ['', Validators.required]
    });
  }

  onBrandChange() {
    this.carService.getModelsForSelectedBrand(this.rForm.get('brand').value)
      .subscribe(
        (data: any) => {
          if (data.length) {
            this.models = data;
          }
        }
      );
  }

  postCar() {
    this.httpClient.post(`http://localhost:3000/api/cars`,
      {
        brand: this.rForm.get('brand').value,
        model: this.rForm.get('model').value,
        engine: this.rForm.get('engine').value,
        enginestats: this.rForm.get('enginestats').value,
        gearbox: this.rForm.get('gearbox').value,
        horsepower: this.rForm.get('horsepower').value,
        tyrebrand: this.rForm.get('tyrebrand').value,
        color: this.rForm.get('color').value,
        year: this.rForm.get('year').value,
        date: this.dateNow,
        ImageUrl: this.rForm.get('ImageUrl').value
      })
      .subscribe(
        (data: any[]) => {
          if (data) {
            console.log('It works');
            this.NavigeteToHome();
          }
        }
      );
  }

  updateCar() {
    this.httpClient.put(`http://localhost:3000/api/cars/${this.carId}`,
      {
        brand: this.rForm.get('brand').value,
        model: this.rForm.get('model').value,
        engine: this.rForm.get('engine').value,
        enginestats: this.rForm.get('enginestats').value,
        gearbox: this.rForm.get('gearbox').value,
        horsepower: this.rForm.get('horsepower').value,
        tyrebrand: this.rForm.get('tyrebrand').value,
        color: this.rForm.get('color').value,
        year: this.rForm.get('year').value,
        date: this.dateNow,
        ImageUrl: this.rForm.get('ImageUrl').value
      })
      .subscribe(
        (data: any[]) => {
          if (data) {
            console.log('It works');
            this.NavigeteToHome();
          }
        }
      );
  }

  getCarBrandsForDropdown() {
    this.carService.getCarBrandsForDropdown()
      .subscribe(
        (data: AllCars[]) => {
          console.log(data);
          this.cars = data;
          console.log(this.cars);
        }
      );
  }

  NavigeteToHome() {
    this.router.navigate(['Home']);
  }

  ngOnInit() {
    this.getCarBrandsForDropdown();
    if (this.carId != null) {
      this.carService.getCarById(this.carId).subscribe((response: Car) => {
        if (response) {
          this.receivedCar = response;
          console.log(this.receivedCar);
          this.rForm.patchValue({
            brand: this.receivedCar.brand,
            model: this.receivedCar.model,
            engine: this.receivedCar.engine,
            enginestats: this.receivedCar.enginestats,
            gearbox: this.receivedCar.gearbox,
            horsepower: this.receivedCar.horsepower,
            tyrebrand: this.receivedCar.tyrebrand,
            color: this.receivedCar.color,
            year: this.receivedCar.year,
            date: this.dateNow,
            ImageUrl: this.receivedCar.ImageUrl
          });
        }
      });

    }
  }

}
