import { CarComponent } from './car/car.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { PublishComponent } from './publish/publish.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/Home',
    pathMatch: 'full'
  },
  {
    path: 'Home',
    component: HomeComponent
  },
  {
    path: 'Publish',
    component: PublishComponent
  },
  {
    path: 'Publish/:id',
    component: PublishComponent
  },
  {
    path: 'Search',
    component: SearchComponent
  },
  {
    path: 'Showcar/:id',
    component: CarComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
