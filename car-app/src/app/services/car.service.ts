import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Car } from './../models/car-model';
import { AllCars } from './../models/cardb-model';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient) {}
  getAllCars(): Observable<Car[]> {
    return this.http.get<Car[]>(`http://localhost:3000/api/cars`);
  }

  getNewestCars(): Observable<Car[]> {
    return this.http.get<Car[]>(`http://localhost:3000/api/cars/newest`);
  }

  getModelsForSelectedBrand(brand: string): Observable<AllCars> {
    return this.http.get<AllCars>(`http://localhost:3000/api/carsForDropdown/${brand}`);
  }

  getCarBrandsForDropdown(): Observable<AllCars[]> {
    return this.http.get<AllCars[]>(`http://localhost:3000/api/carsForDropdown`);
  }

  getCarById(carId): Observable<Car> {
    return this.http.get<Car>(`http://localhost:3000/api/cars/${carId}`);
  }

  deleteCarById(carId): Observable<Car> {
    return this.http.delete<Car>(`http://localhost:3000/api/cars/${carId}`);
  }
}
