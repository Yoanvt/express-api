import { Component, OnInit } from '@angular/core';

import { CarService } from './../services/car.service';
import { AllCars } from './../models/cardb-model';
import { Car } from './../models/car-model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  receivedAllCars: AllCars[];
  receivedModels: string[];
  receivedCars: Car[];

  noCarsFound: Boolean;
  selectedBrand: string;
  selectedModel: string;
  selectedEngine: string;
  selectedYear: string;
  selectedGearbox: string;

  constructor(private carService: CarService) { }

  getCarBrandsForDropdown() {
    this.carService.getCarBrandsForDropdown()
      .subscribe(
        (resoponse: AllCars[]) => {
          this.receivedAllCars = resoponse;
          // console.log(data);
        }
      );
  }

  onBrandChange() {
    this.carService.getModelsForSelectedBrand(this.selectedBrand)
      .subscribe(
        (response: any) => {
          this.receivedModels = response;
          // console.log(response);
        }
      );
  }

  getSearchedCars() {
    this.carService.getAllCars().subscribe( res => {
      if (res) {
        this.noCarsFound = false;
        this.receivedCars = res;
        if (this.selectedBrand ) {
          this.receivedCars = this.receivedCars.filter(car => car.brand === this.selectedBrand );
        } if (this.selectedModel) {
          this.receivedCars = this.receivedCars.filter(car => car.model === this.selectedModel );
        } if (this.selectedEngine) {
          this.receivedCars = this.receivedCars.filter(car => car.engine === this.selectedEngine );
        } if (this.selectedGearbox) {
          this.receivedCars = this.receivedCars.filter(car => car.gearbox === this.selectedGearbox );
        } if (this.selectedYear) {
          this.receivedCars = this.receivedCars.filter(car => car.year >= this.selectedYear );
        } if (this.receivedCars.length === 0) {
          this.noCarsFound = true;
        }
      } else {
        console.log('Something went wrong...');
      }
    });
  }

  ngOnInit() {
    this.getCarBrandsForDropdown();
  }

}
