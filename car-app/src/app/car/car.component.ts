import { CarService } from './../services/car.service';
import { Car } from './../models/car-model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  carId: string;
  car: Car;

  constructor(private activatedRoute: ActivatedRoute, private carService: CarService, private router: Router) {
    this.carId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  deleteCar() {
    this.carService.deleteCarById(this.carId).subscribe( (response: Car) =>  {
      if (response) {
         console.log('Car was deleted');
         this.router.navigate(['Home']);
      } else {
        console.log('Something went wrong');
      }
    });
    console.log(this.carId);
  }

  editCar() {
    this.router.navigate(['Publish', this.carId]);
  }
  ngOnInit() {
    this.carService.getCarById(this.carId).subscribe((response: Car) => this.car = response);
  }

}
