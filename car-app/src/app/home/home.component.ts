import { Component, OnInit } from '@angular/core';

import { CarService } from './../services/car.service';
import { Car } from './../models/car-model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  Cars: Car[];

  constructor(private carService: CarService) { }

  getCars() {
    this.carService.getNewestCars()
    .subscribe(
      (response: Car[]) => {
       if (response.length) {
         this.Cars = response;
       } else {
         console.log('Something went wrong');
       }
      }
    );
  }


  ngOnInit() {
    this.getCars();
    setTimeout(() => console.log(this.Cars), 2500);
  }

}
