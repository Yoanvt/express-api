const express = require('express');
const app = express();
const fs = require('fs');
const Joi = require('joi');

app.use(express.json());

// <- READ FILES ->
fs.readFile('./cars.json', 'utf-8', function(err, data) {
	if (err) throw err
    carsFile = JSON.parse(data)
});

fs.readFile('./carsForDropdown.json', 'utf-8', function(err, data) {
	if (err) throw err
    carsForDropdownFile = JSON.parse(data)
});
// <- READ FILES END ->

// <- API cars for dropdowns endpoints ->
app.get('/api/carsForDropdown', (req, res) => { //Ready
    const brands = [];
    carsForDropdownFile.allcars.forEach(element => {
        brands.push(element.brand);
    });
    sortedBrands = brands.sort();
    if(!sortedBrands) {
        res.status(404).send('No cars found');
    } else {
        res.send(sortedBrands);
    }
});

app.get('/api/carsForDropdown/:brand', (req, res) => { //Ready
    const car = carsForDropdownFile.allcars.find(c => c.brand === req.params.brand);
    if(!car) {
        res.status(404).send('The car with the given brand was not found');
    } else {
        res.send(car.models);
    }
});
// <- API cars for dropdowns endpoints END->

// <- API cars endpoints->
app.get('/api/cars', (req, res) => {
    res.send(carsFile.cars);
});

app.get('/api/cars/newest', (req, res) => { //Ready
    const cars = [];
    sortedCars = carsFile.cars.sort(function(car1,car2) {
        return car2.date - car1.date;
    });
    for (let index = 0; index <= 3; index++) {
        cars.push(sortedCars[index]);
    }
    if(!cars){
        res.status(404).send('No cars found');
    } else {
        res.send(cars);
    }
});

app.get('/api/cars/:id', (req, res) => { //Ready
    const car = carsFile.cars.find(c => c.id === parseInt(req.params.id));
    if(!car) {
        res.status(404).send('The car with the given id was not found');
    } else {
        res.send(car);
    }
});

app.post('/api/cars', (req,res) => { //Ready
    const { error } = validateCar(req.body);
    if(error) return res.status(400).send(error.details[0].message);
        
    const car = req.body;
    car.id = carsFile.cars.length;

    carsFile.cars.push(car);
    carsFile.cars.sort(function(car1,car2) {
        return car1.id - car2.id;
    });
    fs.writeFile('./cars.json', JSON.stringify(carsFile, null, 4), 'utf-8', function(err) {
    if (err) throw err
    console.log('Done!');
    });
    res.send(car);
});

app.put('/api/cars/:id', (req,res) => {
    let car = carsFile.cars.find(c => c.id === parseInt(req.params.id));
    if(!car) return res.status(404).send('The car with the given id was not found');

    const { error } = validateCar(req.body);
    if(error) {
        res.status(400).send(error.details[0].message);
        return ;
    }

    car = req.body;
    car.id = +req.params.id;
    let index = carsFile.cars.findIndex(item => +item.id === car.id);
    carsFile.cars.splice(index, 1, car);

    fs.writeFile('./cars.json', JSON.stringify(carsFile, null, 4), 'utf-8', function(err) {
    if (err) throw err
    console.log('Done!');
    });

    res.send(car);
});


app.delete('/api/cars/:id', (req,res) => {
    let car = carsFile.cars.find(c => c.id === parseInt(req.params.id));
    if(!car) return res.status(404).send('The car with the given id was not found');

    const index = carsFile.cars.indexOf(car)
    carsFile.cars.splice(index, 1);
    carsFile.cars.forEach(element => {
        element.id = carsFile.cars.indexOf(element);    
    });

    fs.writeFile('./cars.json', JSON.stringify(carsFile, null, 4), 'utf-8', function(err) {
    if (err) throw err
    console.log('Done!');
    });

    res.send(car);
});
// <- API cars endpoints END->

// <- Functions ->
function validateCar(car) {
    const schema = {
        brand: Joi.string().required(),
        model: Joi.string().required(),
        engine: Joi.string().min(3).required(),
        enginestats: Joi.string().min(3).required(),
        gearbox: Joi.string().min(3).required(),
        horsepower: Joi.string().min(3).required(),
        tyrebrand: Joi.string().min(3).required(),
        color: Joi.string().min(3).required(),
        year: Joi.string().min(3).required(),
        date: Joi.required(),
        ImageUrl: Joi.required()
    }

    return Joi.validate(car, schema);
}
// <- Functions END->

app.listen(3000, () => console.log('Listening on port 3000...'));